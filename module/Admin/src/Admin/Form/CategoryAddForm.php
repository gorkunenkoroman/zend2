<?php

namespace Admin\Form;

use Zend\Form\Form;

//use Zend\InputFilter\Factoty as InputFactory;
//use Zend\InputFilter\InputFactory;

class CategoryAddForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('categoryAddForm');
        $this->setAttribute('metod', 'post');
        $this->setAttribute('class', 'bs-example form-horizontal');

        $this->add(array(
            'name' => 'categoryKey',
            'type' => 'Text',
            'options' => array(
                'min' => 3,
                'max' => 100,
                'label' => 'Ключ',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'categoryName',
            'type' => 'Text',
            'options' => array(
                'min' => 3,
                'max' => 100,
                'label' => 'Название',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Сохранить',
                'class' => 'btn_submit',
                'required' => 'btn brn-primary',
            ),
        ));
    }
}